import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { PipePotenciaPipe } from './pipe-potencia.pipe';

@NgModule({
  declarations: [
    AppComponent,
    PipePotenciaPipe
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
