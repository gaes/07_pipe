import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `<div class='container'>The hero's birthday is {{ birthday | date }}</div>
             <div class='container'>The hero's birthday is {{ birthday | date:"dd/MM/yyyy" }}</div>
             <div class='container'>The hero's birthday is {{ 2  | pipePotencia:10 }}`
})
export class AppComponent {
  birthday = new Date(1988, 3, 15); // April 15, 1988
}