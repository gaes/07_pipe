import { PipePage } from './app.po';

describe('pipe App', () => {
  let page: PipePage;

  beforeEach(() => {
    page = new PipePage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
